package com.louis.simpleform;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static android.R.attr.button;

/**
 * Created by St. Louis on 01-Jun-17.
 */

public class ButtonClickActivity extends Activity  implements View.OnClickListener {

    Button Submit;
    TextView f_name;
    TextView l_name;
    TextView Gender;
    TextView Thank_You;
    EditText FirstName;
    EditText LastName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.wigout_layout);
        super.onCreate(savedInstanceState);

        Button Submit = (Button) findViewById(R.id.Submit);
        f_name = (TextView) findViewById(R.id.f_name);
        l_name = (TextView) findViewById(R.id.l_name);
        Gender = (TextView) findViewById(R.id.Gender);
        FirstName = (EditText) findViewById(R.id.edit_fname);
        LastName = (EditText) findViewById(R.id.edit_lname);
        Thank_You = (TextView) findViewById(R.id.Thank_You);

        Submit.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {


    }

    public void Submit(View button){
        String FirstName = f_name.getText().toString ();
        String LastName = l_name.getText().toString();
        Thank_You.setText("Hello, " + FirstName);
    }
}
